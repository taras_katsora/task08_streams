package com.epam.task2;
import com.epam.task2.Controller.Controller;

import com.epam.task2.model.Model;
import com.epam.task2.View.View;
import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        Model model = new Model();
        Controller controller = new Controller(model);
        View view = new View(controller);
        view.start();
    }
}
