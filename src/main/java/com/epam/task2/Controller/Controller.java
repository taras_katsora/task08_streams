package com.epam.task2.Controller;

import com.epam.task2.model.Model;

public class Controller {

    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public String executeCommand(String command, String argument) {
        return model.executeCommand(command, argument);
    }
}
