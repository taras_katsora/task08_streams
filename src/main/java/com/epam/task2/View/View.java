package com.epam.task2.View;

import com.epam.task2.Controller.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class View {

    private static List<String> menu = new ArrayList<>();

    static {
        menu.add("<LAMBDA>    - Command as lambda function.");
        menu.add("<REFERENCE> - Command as method reference.");
        menu.add("<ANONYMOUS> - Command as anonymous class.");
        menu.add("<CLASS>     - Command as object of command class.");
        menu.add("<Q>         - Quit.");
    }

    private Controller controller;
    private BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));

    public View(Controller controller) {
        this.controller = controller;
    }


    public void start() throws IOException {
        do {
            showMenu();
            String command = reader.readLine().toUpperCase();
            if (command.equals("Q")) {
                break;
            }
            String argument = reader.readLine();
            System.out.println("-----------------------------------------");
            System.out.println(controller.executeCommand(command, argument));
            System.out.println("-----------------------------------------");
        } while (true);
    }

    private void showMenu() {
        System.out.println("Enter Command:");
        System.out.println("-----------------------------------------");
        for (String str : menu) {
            System.out.println(str);
        }
        System.out.println("-----------------------------------------");
    }
}