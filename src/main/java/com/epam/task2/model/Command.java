package com.epam.task2.model;


public interface Command {

    String execute(String argument);
}
