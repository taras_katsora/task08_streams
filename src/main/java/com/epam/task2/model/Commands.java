package com.epam.task2.model;

public enum Commands {
    LAMBDA,
    REFERENCE,
    ANONYMOUS,
    CLASS
}