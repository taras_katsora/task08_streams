package com.epam.task2.model;

public class Model {

    private Command[] commands;

    public Model() {
        commands = new Command[4];
        commands[0] = (arg) -> "Return \"" + arg + "\" from lambda command.";
        commands[1] = this::getStringForMethodRef;
        commands[2] = getCommandCreatedAsAnonymousClass();
        commands[3] = new SomeCommand();
    }

    public String executeCommand(String command, String argument) {
        Integer id = getCommandId(command);
        return id == null ?
                "No such command." :
                commands[id].execute(argument);
    }

    private Integer getCommandId(String command) {
        Commands enumCommand = null;
        try {
            enumCommand = Commands.valueOf(command);
        } catch (IllegalArgumentException e) {
            return null;
        }
        return enumCommand.ordinal();
    }

    private String getStringForMethodRef(String argument) {
        return "Return \"" + argument + "\" from method reference command.";
    }

    private Command getCommandCreatedAsAnonymousClass() {
        return new Command() {
            @Override
            public String execute(String argument) {
                return "Return \"" + argument + "\" from anonymous class command.";
            }
        };
    }
}