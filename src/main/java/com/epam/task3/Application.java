package com.epam.task3;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Application {

    public static Integer[] generateArrayOfIntegers(int count) {
        return new Random().ints(count)
                .boxed()
                .toArray(Integer[]::new);
    }

    public static int[] generateArrayOfInts(int count) {
        return new Random().ints(count).toArray();
    }

    public static List<Integer> generateListOfIntegers(int count) {
        return new Random().ints(count)
                .boxed()
                .collect(Collectors.toList());
    }

    public static List<Integer> generateListOfIntWithinTheRange(
            int begin, int end) {
        return new Random().ints(begin, end)
                .boxed()
                .collect(Collectors.toList());
    }

    public static List<Integer> generateListOfIntWithinTheRangeAndSize(
            int size, int begin, int end) {
        return new Random().ints(size, begin, end)
                .boxed()
                .collect(Collectors.toList());
    }

    public static double getAverage(List<Integer> list) {
        return list.stream()
                .mapToInt(i -> i)
                .average()
                .orElse(0);
    }

    public static double getAverage(int[] array) {
        return Arrays.stream(array)
                .average()
                .orElse(0);
    }

    public static int getMin(List<Integer> list) {
        return list.stream()
                .mapToInt(i -> i)
                .min()
                .orElse(0);
    }

    public static int getMin(int[] arrays) {
        return Arrays.stream(arrays)
                .min()
                .orElse(0);
    }

    public static int getMax(List<Integer> list) {
        return list.stream()
                .mapToInt(i -> i)
                .max()
                .orElse(0);
    }

    public static int getMax(int[] arrays) {
        return Arrays.stream(arrays)
                .max()
                .orElse(0);
    }

    public static int getSum(List<Integer> list) {
        return list.stream()
                .mapToInt(i -> i)
                .sum();
    }

    public static int getSum(int[] arrays) {
        return Arrays.stream(arrays).sum();
    }

    public static int getSumByReduce(List<Integer> list) {
        return list.stream()
                .reduce((sum, x) -> sum + x)
                .orElse(0);
    }

    public static int getSumByReduce(int[] arrays) {
        return Arrays.stream(arrays)
                .reduce((sum, x) -> sum + x)
                .orElse(0);
    }

    public static void printSummaryStatistic(List<Integer> list) {
        IntSummaryStatistics stats = list.stream()
                .mapToInt(i -> i)
                .summaryStatistics();
        System.out.format("  count: %d%n", stats.getCount());
        System.out.format("    sum: %d%n", stats.getSum());
        System.out.format("average: %.2f%n", stats.getAverage());
        System.out.format("    min: %d%n", stats.getMin());
        System.out.format("    max: %d%n", stats.getMax());
    }

    public static void main(String[] args) {
        List<Integer> list =
                generateListOfIntWithinTheRangeAndSize(20,-50, 50);
        printSummaryStatistic(list);
        System.out.println("----------------------------------------");
        System.out.format("       sum: %d%n", getSum(list));
        System.out.format("reduce sum: %d%n", getSumByReduce(list));
        System.out.format("   average: %.2f%n",getAverage(list));
        System.out.format("       min: %d%n", getMin(list));
        System.out.format("       max: %d%n", getMax(list));
    }
}
