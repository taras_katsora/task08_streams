package com.epam.task4;

import com.epam.task4.Controller.Controller;
import com.epam.task4.Model.Domain;
import com.epam.task4.Model.Model;
import com.epam.task4.View.View;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        Model model = new Model(new Domain());
        Controller controller = new Controller(model);
        View view = new View(controller);
        view.start();
    }
}