package com.epam.task4.Controller;


import com.epam.task4.Model.Model;

import java.util.List;
import java.util.Map;

public class Controller {

    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void execute(String text) {
        model.execute(text);
    }

    public int getUniqueWords() {
        return model.getUniqueWords();
    }

    public List<String> getUniqueWordsList() {
        return model.getUniqueWordsList();
    }

    public Map<String, Integer> getOccurrenceWords() {
        return model.getOccurrenceWord();
    }

    public Map<Character, Integer> getOccurrenceSymbols() {
        return model.getOccurrenceSymbols();
    }
}