package com.epam.task4.Model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Domain {

    private int uniqueWords;
    private List<String> uniqueWordsList;
    private Map<String, Integer> occurrenceWordsMap;
    private Map<Character, Integer> occurrenceSymbolsMap;
    private String[] wordsArray;

    private String[] getWordsArrayFromText(String text) {
        if (wordsArray == null) {
            wordsArray = text.split(" +");
            return wordsArray;
        }
        return wordsArray;
    }

    public void calcNumberOfUniqueWords(String text) {
        uniqueWords = (int) Arrays.stream(getWordsArrayFromText(text))
                .map(String::toLowerCase)
                .distinct()
                .count();
    }

    public void createSortedListOfUniqueWords(String text) {
        uniqueWordsList = Arrays.stream(getWordsArrayFromText(text))
                .map(String::toLowerCase)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public void calcNumberOfOccurrenceWords(String text) {
        occurrenceWordsMap = Arrays.stream(getWordsArrayFromText(text))
                .collect(Collectors.toMap(
                        k -> k,
                        v -> 1,
                        Integer::sum,
                        HashMap::new
                ));
    }

    public void calcNumberOfOccurrenceSymbols(String text) {
        occurrenceSymbolsMap = Arrays.stream(getWordsArrayFromText(text))
                .flatMap(word -> word.chars().mapToObj(i -> (char) i))
                .filter(i -> i >= 'a' && i <= 'z')
                .collect(Collectors.toMap(
                        k -> Character.valueOf(k),
                        v -> 1,
                        Integer::sum,
                        HashMap::new));
    }

    public int getUniqueWords() {
        return uniqueWords;
    }

    public List<String> getUniqueWordsList() {
        return uniqueWordsList;
    }

    public Map<String, Integer> getOccurrenceWords() {
        return occurrenceWordsMap;
    }

    public Map<Character, Integer> getOccurrenceSymbols() {
        return occurrenceSymbolsMap;
    }
}