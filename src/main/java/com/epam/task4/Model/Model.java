package com.epam.task4.Model;

import java.util.List;
import java.util.Map;

public class Model {

    private Domain domain;

    public Model(Domain domain) {
        this.domain = domain;
    }

    public void execute(String text) {
        domain.calcNumberOfUniqueWords(text);
        domain.calcNumberOfOccurrenceSymbols(text);
        domain.calcNumberOfOccurrenceWords(text);
        domain.createSortedListOfUniqueWords(text);
    }

    public int getUniqueWords() {
        return domain.getUniqueWords();
    }

    public List<String> getUniqueWordsList() {
        return domain.getUniqueWordsList();
    }

    public Map<String, Integer> getOccurrenceWord() {
        return domain.getOccurrenceWords();
    }

    public Map<Character, Integer> getOccurrenceSymbols() {
        return domain.getOccurrenceSymbols();
    }
}