package com.epam.task4.View;

@FunctionalInterface
public interface Printable {

    void print();
}

