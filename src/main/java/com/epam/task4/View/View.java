package com.epam.task4.View;
import com.epam.task4.Controller.Controller;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader reader =
            new BufferedReader(new InputStreamReader(System.in));

    public View(Controller controller) {
        this.controller = controller;

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Number of unique words");
        menu.put("2", "  2 - Sorted list of all unique words");
        menu.put("3", "  3 - Occurrence number of each word in the text");
        menu.put("4",
                "  4 - Occurrence number of each symbol except upper case characters");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    public void start() throws IOException {
        System.out.println("Enter some number of text lines:");
        System.out.println("-------------------------------------------");
        StringBuilder builder = new StringBuilder();
        String inputLine;
        while (!(inputLine = reader.readLine()).isEmpty()) {
            builder.append(inputLine);
        }
        controller.execute(builder.toString());
        show();
    }

    private void printMenu() {
        System.out.println("MENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    private void show() throws IOException {
        String choice = "";
        while (!choice.equals("Q")) {
            System.out.println("-------------------------------------------");
            printMenu();
            System.out.println("Please, select menu point.");
            System.out.println("-------------------------------------------");
            choice = reader.readLine().toUpperCase();
            Printable printable = methodsMenu.get(choice);
            if (printable != null) {
                printable.print();
            }
        }
    }

    private void pressButton1() {
        System.out.println(
                "Number of unique words:\n" + controller.getUniqueWords());
    }

    private void pressButton2() {
        System.out.println(
                "Sorted list of all unique words:\n"
                        + controller.getUniqueWordsList());
    }

    private void pressButton3() {
        System.out.println(
                "Occurrence number of each word in the text:\n"
                        + controller.getOccurrenceWords());
    }

    private void pressButton4() {
        System.out.println(
                "Occurrence number of each symbol except upper case characters:\n"
                        + controller.getOccurrenceSymbols());
    }
}
